import pytesseract
import cv2
import os
import numpy as np
import sys
from util import save_output_img

class ExtractCfg(object):
    tesseract = '-c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ --psm 8 --oem 3'
    char_ratio = 1.2
    char_min_area = 100


def extract(image_path):
    # point to license plate image (works well with custom crop function)
    gray = cv2.imread(image_path, 0)
    gray = cv2.resize(gray, None, fx=3, fy=3, interpolation=cv2.INTER_CUBIC)
    save_output_img('step_01_grayscale', gray)

    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    gray = cv2.medianBlur(gray, 3)
    save_output_img('step_02_blurred', gray)

    # perform otsu thresh (using binary inverse since opencv contours work better with white text)
    ret, thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)
    save_output_img('step_03_otsu_thresh', thresh)

    rect_kern = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    # apply dilation
    dilation = cv2.dilate(thresh, rect_kern, iterations=1)
    save_output_img('step_04_dilation', dilation)

    # find contours
    try:
        contours, hierarchy = cv2.findContours(dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    except:
        ret_img, contours, hierarchy = cv2.findContours(dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    sorted_contours = sorted(contours, key=lambda ctr: cv2.boundingRect(ctr)[0])

    # create copy of image
    im2 = gray.copy()

    plate_num = ""
    roi_idx = 0
    # loop through contours and find letters in license plate
    for cnt in sorted_contours:
        x, y, w, h = cv2.boundingRect(cnt)
        height, width = im2.shape

        # if height of box is not a quarter of total height then skip
        if height / float(h) > 3: continue

        # if height to width ratio is less than 1.2 skip
        ratio = h / float(w)
        if ratio < ExtractCfg.char_ratio: continue

        # if area is less than 100 pixels skip
        area = h * w
        if area < ExtractCfg.char_min_area: continue

        # draw the rectangle
        cv2.rectangle(im2, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # get the sub image for the ROI
        roi = thresh[y - 5:y + h + 5, x - 5:x + w + 5]

        # Invert the image to have it back to white background black letters
        roi = cv2.bitwise_not(roi)

        # Apply noise removal one more time
        roi = cv2.medianBlur(roi, 5)

        # Save result ROI
        roi_idx += 1
        save_output_img('roi_{0}'.format(roi_idx), roi)

        # Send the ROI content to extraction OCR
        text = pytesseract.image_to_string(roi, config=ExtractCfg.tesseract)

        plate_num += text

    print(plate_num.replace('\n', '').replace('\r', ''))
    save_output_img('step_05_segmented_characters', im2)

# ---------------------------------------------------------
if __name__ == "__main__":
    args = sys.argv[1:]
    file_name = args[0]
    extract(file_name)