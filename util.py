import cv2
import os
import numpy as np
from matplotlib import pyplot as plt

def save_output_img(name, img):
    if not os.path.exists('data/out'):
        os.makedirs('data/out')

    file_path = os.path.join('data', 'out', '{0}.jpg'.format(name))
    cv2.imwrite(file_path, img)

def show_plot(title, img):

    if isinstance(img, np.ndarray):
        image = img
    else:
        image = cv2.imread(img)

    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.figure(num=None, figsize=(12, 8), dpi=80, facecolor='w', edgecolor='k')
    plt.title(title)
    plt.imshow(image)
    plt.show()

def show_subplot(titles, images, rows, cols):

    for i in range(len(images)):
        img = images[i]

        if isinstance(img, np.ndarray):
            image = img
        else:
            image = cv2.imread(img)

        if len(image.shape) == 3:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        fig, ax = plt.subplot(rows, cols, i + 1), plt.imshow(image, 'gray')

        plt.title(titles[i])
        ax.set_xlabel('width [px]')
        ax.set_ylabel('height [px]')
        plt.xticks([]), plt.yticks([])

    plt.show()