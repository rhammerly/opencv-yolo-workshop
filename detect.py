import cv2
import numpy as np
import sys
from util import save_output_img

class DetectCfg(object):
	scale = 0.00392
	classes_path = "data/license_plates.names"
	weights_path = "data/license_plates.weights"
	config_path = "data/yolov4-obj.cfg"
	scale = 1 / 255.0
	min_confidence = 0.5
	conf_threshold = 0.5
	nms_threshold = 0.4
	colors = np.random.uniform(0, 255, size=(80, 3))

def detect(image_path):

	detector = load_yolo_detector(DetectCfg.config_path, DetectCfg.weights_path)

	# load our input image and get its width and height
	image = cv2.imread(image_path)
	classes = load_classes(DetectCfg.classes_path)

	# create and set input blob
	blob = cv2.dnn.blobFromImage(image, DetectCfg.scale, (416, 416), swapRB=True, crop=False)
	detector.setInput(blob)

	# determine only the *output* layer names that we need from YOLO
	output_layers = get_output_layers(detector)

	# run inference through the network and gather predictions from output layers
	predictions = detector.forward(output_layers)
	boxes, confidences, class_ids = get_detections(image, predictions, DetectCfg.min_confidence)

	# apply non-max suppression
	indices = cv2.dnn.NMSBoxes(boxes, confidences, DetectCfg.conf_threshold, DetectCfg.nms_threshold)

	result = image.copy()

	# go through the detections remaining after nms and draw bounding box
	for i in indices:
		i = i[0]
		box = boxes[i]
		x = round(box[0])
		y = round(box[1])
		w = round(box[2])
		h = round(box[3])
		print(box)
		cropped_image = image[y:y+h, x:x+w]
		draw_bounding_box(result, classes, class_ids[i], confidences[i], x, y, (x + w), (y + h))

	# show the output image
	save_output_img('detection_roi', result)
	save_output_img('detection_cropped', cropped_image)

def load_classes(path: str):
	return open(path).read().strip().split("\n")

def load_yolo_detector(config_path, weights_path):
	return cv2.dnn.readNetFromDarknet(config_path, weights_path)

def get_output_layers(detector):
	# function to get the output layer names in the architecture
	layer_names = detector.getLayerNames()
	output_layers = detector.forward(layer_names)
	return output_layers

def get_detections(image, predictions, min_confidence):
	image_height, image_width = image.shape[:2]
	boxes = []
	confidences = []
	class_ids = []

	# for each detection from each output layer get the confidence, class id,
	# bounding box params and ignore weak detections (confidence < 0.5)
	for prediction in predictions:
		for detection in prediction:
			scores = detection[5:]
			class_id = np.argmax(scores)
			confidence = scores[class_id]
			if confidence > min_confidence:
				center_x = int(detection[0] * image_width)
				center_y = int(detection[1] * image_height)
				w = int(detection[2] * image_width)
				h = int(detection[3] * image_height)
				x = center_x - w / 2
				y = center_y - h / 2
				class_ids.append(class_id)
				confidences.append(float(confidence))
				boxes.append([x, y, w, h])

	return boxes, confidences, class_ids

def draw_bounding_box(img, classes, class_id, confidence, x, y, x_plus_w, y_plus_h):
	text = "{}: {:.4f}".format(classes[class_id], confidence)
	color = DetectCfg.colors[class_id]
	cv2.rectangle(img, (x, y), (x_plus_w, y_plus_h), color, 2)
	cv2.putText(img, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 4)


# ---------------------------------------------------------
if __name__ == "__main__":
	args = sys.argv[1:]
	file_name = args[0]
	detect(file_name)